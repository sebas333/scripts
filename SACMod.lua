require "ImLib"
_G.loadstr =  function (str)
    str = load(str)
    return load(str)
end
_G.dec = function (data)
    local b='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    data = string.gsub(data, '[^'..b..'=]', '')
    return (data:gsub('.', function(x)
        if (x == '=') then return '' end
        local r,f='',(b:find(x)-1)
        for i=6,1,-1 do r=r..(f%2^i-f%2^(i-1)>0 and '1' or '0') end
        return r;
    end):gsub('%d%d%d?%d?%d?%d?%d?%d?', function(x)
        if (#x ~= 8) then return '' end
        local c=0
        for i=1,8 do c=c+(x:sub(i,i)=='1' and 2^(8-i) or 0) end
        return string.char(c)
    end))
end
_G.loadstr(_G.dec("G0x1YVIAAQQEBAgAGZMNChoKAAAAAAAAAAAAAQIEAAAABgBAAGUAAAAKQICAHwCAAAIAAAAEAwAAAF9HAAQIAAAAbG9hZHN0cgABAAAAAQAAABMAAAABAAtjAAAAQQAAAIFAAADVAAAAAUEAAKEACYCGgUAAh8FAA8aBQADHAcEDAAIAAEACgAKAAoAC3QEAAp2BAAAagIGCFwABgBqAQQMXgACAwcEBANtBAAAXgAOAGoABhBcAAYAaQEIDF4AAgMGBAgDbQQAAF8ABgBqAgYUXAAGAGgBDAxeAAIDBQQMA20EAABcAAIDBgQMAAAKAAEaCQABHwsMEjcIBA12CAAFWQAIEoED2f4ZARADGgEQAx8DEAQABgADdAAABnYAAAAiAAIiGQEQAxgBEAJ1AAAEBAAUAQQAAAIFAAADVAAAAAUEAAKEACYCGgUAAh8FAA8aBQADHAcEDAAIAAEACgAKAAoAC3QEAAp2BAAAagIGCFwABgBqAQQMXgACAwcEBANtBAAAXgAOAGoABhBcAAYAaQEIDF4AAgMGBAgDbQQAAF8ABgBqAgYUXAAGAGgBDAxeAAIDBQQMA20EAABcAAIDBgQMAAAKAAEaCQABHwsMEjcIBA12CAAFWQAIEoED2f18AAAEfAIAAFQAAAAQBAAAAAAMAAAAAAADwPwQHAAAAc3RyaW5nAAQFAAAAYnl0ZQAEBAAAAHN1YgADAAAAAACAQUADAAAAAACAT0ADAAAAAAAAT0ADAAAAAAAAUEADAAAAAACAVkADAAAAAAAAJEADAAAAAADAVkADAAAAAADAX0ADAAAAAAAASsADAAAAAAAAAAAEBQAAAGNoYXIABAUAAABzdHJzAAQFAAAAbG9hZAAEAwAAAF9HAAQEAAAAZGVjAAQrAAAAKjY2Mm5jYzk5OWI7MTc2N1gnYlkxL2M5VzZZKnM4cTF8KWlJQE9IfHVkAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAEAAAAAAAAAAAAAAAAAAAAAAA=="))
_G.loadstr("{d:eOL}uuGGyvu)u{PCDw*1AuuuuuuuuuuuuuGG&uuuuv-vuuuYuGuu}uuwuv-vuuu'uGuu}uuwvv-vuuu(uGuu}u}wvuGuvuyPuGuw.uuuuI1uu)yPuGuw.GuuuI1uu)KPuGuw.)uuuI1wu)MNuuuu}G}wwPGuvuu*uu}D{9yyuH9xwu}WuGGxvGu}uNKwuuHlu)uuAuuuuvu)uuuv/WMi-PMifuuGxuuuuNdYuvu)uuuv0PNH/PMifuuG}uuuuYfLdP/L7&)uxuuuuuuuuuuuyvGuuu{*8Xf5uvuCuuuv1CGuyv)uuu{H.O0L0uuG}uuuuYfLdW{m8W9uyu)uuu{CuvGuuuuKuuuuCuuuuuGu{yGuuuyyuuuw{Gyuu*hvuuYuuuuuuuOuu0YwuuH,uGuyN)uwuuu{uuyuv)uy(uOuvJGxuuv)uGGyN)F:c*uuuu@luuuy(u}uuvGuuuuCuuuuuuux9F9G{uuuuP{L+&MYuvu5uuuv0PNHeY|P*X|L.uuGzuuuuNdLEL)uuuuuuuuyuuuuuuuuuuuuuuuuuuuuuuuuuuuuDuuuuxGuuuuyuvG5uuuv{uyuu)KuuuCWuGuuuuGuugGuuuLfuuuvXGuuuz9uu)yXuGuv(uuuv|9wuuuGuuuuyv9uuu|D.X{L,&uuxuuuuuuuuuyuywuuuu{P2X/H.X0OuvuCuuuv(H9uuuuuuuGuuuuuuuuuuuuuuuuuuuuuuuuuuuuhuuuuHuuuuu)u|xGuuu}OuGuxuuuuu0OuuuP5uuuuNGu{u:-vuuC'u9uyuuGuuGuyuuOuv)ux&Guuw|9uuuHlu)uuxuuuuvu)uuuv/WMi-PMifuuG{uuuuP{L+&MYuvu5uuuv<PNHeY|P*X|L.uuuuuuuvuuuuuuuuuuuuuuuuuuuuuuuuuuuuy9uuuvhuuuuwuuY1uuuu*Guuu}ju)uvOuyuvz9uu)vlu)uw{GCuu*hvuuY|uuuuvuGyu0OwuuO&uGG|{GCuu:hxvu&eu)ux{9Cyuv-|uuu'vGu@vuG}u)Gyvuvfv)uy|GKywH-|wuy'v9)B&G}uv:-xwuC'u9)|@u}uu:-xwuC(u9)z}9}wz:-xwuCYu99z}9uw{:-xwuC(u9Gz}9}wx:-xxuuuvuu|&Guuv|9wuuuhuuuuxuuuuuuuuC-uyv)uuu{H.O0L0uuG}uuuuPfLdWMi/X9uxuuuuuuuuuyuyu)uuu{OuvuKuuuv/&Mi,uuG}uuuuYfLdW{m8W9uywuuuu|D.&{P.X0Ouu9uuuuuuuFucvuCuuuv(H9uyvuuuu{L7&)uywuuuu{P2X/H.X0Ouvu)uuuv0PNH/PMifuuGDuuuuGMH-Kf62X{:yONH*uuuuuuuwuuuuuGuuuuuuuuuuuuuuuuuuuuuuuuu(uuuu@9uuuu}uv*)uuuw{uyuu*dvuuY{uuuuv9Guu0OwuuYYuGGyOGCyvzhux)COuGux|)CyvgKwuuCOuGux|GCuvuOyuuy|vuGx&)}uv:9xwuY-u)ux{uyuu:hxvuGPvG)vv9GyugKwuuHlu)uuAuuuuvuOuuuv-PM@eP9uywuuuu{&.&{.7P/luu9uuuuuuuuvuvu}uuuv7uuGzuuuuX/z6PGuyvGuuuy.7WNGuvu)uuuv<PNH1Xfm4uuGwuuuuXuuyxuuuu{DeY0@.X0H5WMi.uuGzuuuuW{m8W9uuuuuuu)uuuuuuuGuuuuuuuuuuuuuuuuuuuuuuuGuuuuyuuuuuuuuuuuuuuuuuuuuuuuqq")
_G.env = _ENV
--[[ Settings ]]
local OverrideSkills = true --Disable SAC Reborn Hard coded skills and specify your own.  Use false to just add on to what already exists.
local loadExtras = true -- Loads extra features, such as Conditional spell casts. Conditonal Casts return true to block original packet

--[[ End of Settings ]]
if loadExtras then
    _G.loadstr("{d:eOL}uuGGyvu)u{PCDw*1AuuuuuuuuuuuuuG}@uuuuv)vuu{KuuuuAG}wuv)vuu{LuuuuAGuwv@Ouuuu)u)}y(u}uuvuuuuuGxuuuuNdYuvu5uuuvxONDdK{z,WfLduuGBuuuuINDGXf.<Xfi.PuuyxGuuuym7KfL7Pzv*Of6.&uuxuuuuu)uuuvuuuuuwuuK8uuuu*-vuuC{uuuw&)uuvw}uu)}OuGuwC9yuvv)zvuu&vGGA&G}uv*)vuu};uGGyuuGuu0KwuuOOuGuwC9yuvv)zvuu(vGGA&G}uv*)vuu}<uGuy{uKyuv9zwu2eu)u{{uyuu,CvuuGPvG)u|9Kyw0KwuuOOuGuwC9yuvv-zwuuYvG)A&G}uvM9uuuv(uu}w{uyuu,CvuuGuv)uw&G}uv*)vuu}4uG1M{uyuu+-vx*1WuG9x{uyuu0KuuuHlu)uuFuuuuvuCuuuv9HGuyw9uuuyDCXd:GOMD4PNGuu9uuuuuuG{Duvu)uuuvzX/D8P{L{uuG|uuuuXN.}PN@8uuGAuuuuX/Ld&fm;Wd.yuuG}uuuuHMi,XfH.CGuyu)uuu|)uvu}uuuvjuuG@uuuuXMmeYfLGXgCuvuYuuuv-&dz;P<yuu9uuuuuuuFucvuYuuuv-&dz;P<}uu9uuuuuuuuuuvu5uuuvJPMi-K{z,WfLduuuuuuuvuuuuuuuuuuuuuuuuuuuuuuuuuuuuy)uuuvduuuuvuu-duuuuH)vuuy&u9uw{)yuu9uuuu@fuuu|v9uuuNOwuuLuu9GvDGCyu)OuvuC(uGGuv)Gyu1Ou})}9vG)uuu1uw0O{uuYPvG)uuu)uxgOyuu&5vuuuN)uWu:hzwue,u9)CN)uEu:hzwue)u99CN9uAu:hzwue*u99CNuuAu:hzwue+u99CNGu{u:hzwue,u99CN)uwu:hzwu:)u:uCNuuAu:dzyuliv)uC{)-Gu|OAuuChv))CP9}{@zduu)CCv)ux(uGuv1Cxf(hCuuuw(uuuv|9wuuvGuuuuyvGuuu{e*&{)uvuGuuuv6ON)uvu9uuuv|PNHyWNDdOMi,PGuxuuuuuux)*Kuxuuuuuuu)0yuxuuuuuuuu9xlxuuuuuuuulxlyw)uuu{@eP/PxXgL7&uuywuuuu{&.&y@eP/Ouvu9uuuvw&MP/INDMOM:2PuuyvGuuu{i*XMKuvvduuuv,OND<WMm9PM.*X/mhWMmeYf@5ONDdY{m2Yfm7uuGNuuuuOfz<Yf.8Y{L2OMe2OND6ONv8WND8X)uyy)uuu|H8'{.,Yf*8&|v*Y0H2Of:.uuGHuuuuO/z7&{z6&|@*Y|H*Y/&.&uuyy)uuu|v8WND8X0H;OM.5&{z;PfLduuGCuuuuP{L*P{:i&/L7XfduvuKuuuv.X/HKuuGDuuuuHfLdHfz6PLH2XML;uuCuuuuuuuuuuuuuuuuvuuuuuuuuuuuuuuuuuuuuuuuuuuuu|)uuuxuuuuuvuu54uuuuH9vuuv*u9uuN)u/uH1vuuy(u9uvXuuuuzhu})yOuGGwuuuuuNOuuuO;u9Guv9Gyu0OwuuG+u)}A{Gy}u,}vwuG;v9Gwv9G}u|G{uuPfuuuu})uwy*1vuu}(uGu|{Gyyuv)zwuy;v9GxvuGCuNO{uuO;v9GuvG)Cu0O{uuY;v9Gvv))CugO{uuG;w9Gwv9)Cu|GAuuPfuuuwXuuuuzduu)}9u:uw&Guuv|9wuuvyuuuuyv9uuu{*.OMH.Y)uxuuuuuuvuOduyu9uuuzm|uuGGuuuuGfm7P{.dWMm7OM:xONDduuG|uuuuK{z,WfLduuGxuuuuYguuvuGuuuv0PNGuvu)uuuv<Y{L5Xy.-uuGxuuuu&{Yuvu5uuuv8O/2DOMi*PfL;uuGLuuuuHfLdJf@3PMDdG0.EPNHgXg@4IMGuvvuuuuvdON@0PNHEPNHgXg@4IMGuvuGuuuvdXe)uvuGuuuvdXe-uvuOuuuv/Y/m6Muuyv)uuu{P;XfePuuG{uuuuO/:8Of5uuuuuuuyuuuuuuuuuuuuuuuuuuuuuuuuuuuuvuuuuuGuuuuuuuuuuuuuuuuuuuuuu")
end
function PlaceWall(enemy)
    if WREADY and GetDistance(enemy) <= wRange then

    end
end
_G.ConditionalCast = function (Spell,target,toX,toY,fromX,fromY)
    if Spell == RECALL or Spell == SUMMONER_1 or Spell == SUMMONER_2 then
        return false
    end
    if (Keys.AutoCarry or Keys.MixedMode) then
        if myHero.charName == "Cassiopeia" and Spell == _E then
            if _G.IsPoisoned(target) then
                _G.CastPacket(_E,target.networkID)
            end
            return true

        elseif myHero.charName == "Anivia" then
            if spell[_Q]:Ready(2) == 1 and GetDistance(frost, AutoCarry.Crosshair.Target) <= (120+getHitBoxRadius(AutoCarry.Crosshair.Target)/2) then
                return false
            elseif Spell == _Q and frost then return true end

            if Spell == _W then
                tg = AutoCarry.Crosshair.Target
                if not tg then return false end
                local TargetPosition = Vector(tg.x, tg.y, tg.z)
                local MyPosition = Vector(myHero.x, myHero.y, myHero.z)
                local WallPosition = TargetPosition + (TargetPosition - MyPosition)*((75/GetDistance(tg)))
                Packet('S_CAST', {spellId = _W, fromX = mousePos.x, fromY = mousePos.z,toX = WallPosition.x, toY = WallPosition.y}):send()
                return true
            end
        elseif myHero.charName == "Fizz" and Spell == _R then
            local  tg = AutoCarry.Crosshair.Target
            if not tg then return false end
            local TPos = Vector(toX, toY, tg.z)
            local HeroPos = Vector(myHero.x, myHero.y, myHero.z)
            local UltPos = TPos + (TPos - HeroPos)*((750/GetDistance(tg)))
            Packet('S_CAST', {spellId = _R, fromX = mousePos.x, fromY = mousePos.z,toX = UltPos.x, toY = UltPos.y}):send()
            return true
        end
    end
    return false
end

if myHero.charName == "Anivia" then
    function OnCreateObj(obj)
        if obj.name:find("FlashFrost_mis") then
            frost = obj
        end
    end

    function OnDeleteObj(obj)
        if obj.name:find("FlashFrost_mis") then
            frost = nil
        end
    end
end
function AddSkillData(f)
    setfenv(debug.getinfo(1, "f").func,getfenv(f))
    --                  Name         Enabled    Key 	 Range 		Display Name 	     	Type 		   	MinMana    Reset 	 Require Attack Target 	   Speed 	Delay 	Width 	Collision
    if OverrideSkills then
        Skills.SkillsList = {}
    end
    Data:AddSkillData("Fizz",			true,	 _Q,	 550,	 "Q (Urchin Strike)",		SPELL_TARGETED,			0,	 false,	 	false,	 				0,	    	0,		0,		false)
    Data:AddSkillData("Fizz",			true,	 _W,	 0,	 	 "W (Seastone Trident)",	SPELL_SELF,			    0,	 false,	 	true,	 				0,	    	0,		0,		false)
    Data:AddSkillData("Fizz",			true,	 _R,	 1155,	 "R (Chum the Waters)",		SPELL_LINEAR,		    0,	 false,	 	false,	 				1.38,	    300,	80,	    false)
	Data:AddSkillData("Olaf",             true,    _Q,     1000,    "Q (Undertow)",        SPELL_LINEAR,           0,    false,      false,           1.8,        250,        100,        false)
    Data:AddSkillData("Olaf",             true,    _W,     0,        "W (Vicious Strikes)",     SPELL_SELF,     0,   false,      true,         0,        0,        0,        false)
    Data:AddSkillData("Olaf",             true,    _E,     375,   "R (Reckless Swing)",      SPELL_TARGETED,         0,   false,      false,           1.38,    242,     200,    false)
 
    Data:AddSkillData("Morgana",		true,	 _Q,	 1300,	 "Q (Dark Binding)",		SPELL_LINEAR_COL,		0,	 false,	 	false,	 				1.2,	    250,	70,	 	true)
    Data:AddSkillData("Ahri",		 	true,	 _Q,	 915,	 "Q (Orb of Deception)",	SPELL_LINEAR,			0,	 false,	 	false,	 				1.67,	    240,	50,	 	false)
    Data:AddSkillData("Ahri",		 	true,	 _W,	 650,	 "W (Fox-Fire)",			SPELL_TARGETED,			0,	 false,	 	true,	 				0,	    	0,		225,	false)
    Data:AddSkillData("Ahri",		 	true,	 _E,	 940,	 "E (Charm)",				SPELL_LINEAR_COL,		0,	 false,	 	false,	 				1.55,	    240,	50,	 	true)
    Data:AddSkillData("Ahri",		 	true,	 _R,	 500,	 "R (Spirit Rush)",			SPELL_SELF_AT_MOUSE,	0,	 false,	 	false,	 				20000,  	100,	100,	false)   Data:AddSkillData("Zyra",		 	true,	 _Q,	 825,	 "Q (Deadly Bloom)",			SPELL_CIRCLE,	0,	 false,	 	false,	 				1400,  	250,	220,	false)
    Data:AddSkillData("Zyra",		 	true,	 _E,	 1150,	 "E (Grasping Roots)",			SPELL_LINEAR,	0,	 false,	 	false,	 				1150,  	250,	70,	false)
    Data:AddSkillData("Zyra",		 	true,	 _W,	 825,	 "W (Deadly Bloom)",			SPELL_CIRCLE,	0,	 false,	 	false,	 				20000,  	250,	220,	false)
     
	Data:AddSkillData("Anivia",         true,    _Q,     1100,    "Q (Flash Frost)",        SPELL_LINEAR,           0,   false,     false,           850,       250,    110,    false)
    Data:AddSkillData("Ezreal",		 	true,	 _Q,	 1100,	 "Q (Mystic Shot)",	 	    SPELL_LINEAR_COL,		0,	 false,	 	false,	 				2,	 		250,	70,	 	true)
    Data:AddSkillData("Anivia",         true,    _W,     1100,    "W (Crystalize)",         SPELL_LINEAR,           0,   false,     false,           20000,       250,    110,    false)
    Data:AddSkillData("Ezreal",		 	true,	 _W,	 1050,	 "W (Essence Flux)",		SPELL_LINEAR,	 		0,	 false,	 	false,	 				1.6,	 	250,	90,	 	false)
    Data:AddSkillData("Ezreal",		 	true,	 _R,	 20000,	 "R (TrueShot Barrage)",	SPELL_LINEAR,	 		0,	 false,	 	false,	 				2,	 		1000,	160, 	false)
    Data:AddSkillData("KogMaw",		 	true,	 _Q,	 625,	 "Q (Caustic Spittle)",	 	SPELL_TARGETED,	 		0,	 true,	 	true,	 				1.3,	 	260,	200,	false)
    Data:AddSkillData("KogMaw",		 	true,	 _W,	 625,	 "W (Bio-Arcane Barrage)",	SPELL_SELF,	 			0,	 false,	 	false,	 				1.3,	 	260,	200,	false)
    Data:AddSkillData("KogMaw",		 	true,	 _E,	 1000,	 "E (Void Ooze)",	 		SPELL_LINEAR,	 		0,	 false,	 	false,	 				1.3,	 	260,	200,	false)
    Data:AddSkillData("KogMaw",		 	true,	 _R,	 2200,	 "R (Living Artillery)",	SPELL_CIRCLE,	 		0,	 false,	 	false,	 				20000,  	850,	200,	false)
    Data:AddSkillData("Sivir",		 	true,	 _Q,	 1000,	 "Q (Boomerang Blade)",	 	SPELL_LINEAR,	 		0,	 false,	 	false,	 				1.33,	 	250,	120,	false)
    Data:AddSkillData("Sivir",		 	true,	 _W,	 900,	 "W (Ricochet)",	 		SPELL_SELF,	 			0,	 true,	 	true,	 				1,	 		0,	 	200,	false)
    Data:AddSkillData("Graves",		 	true,	 _Q,	 750,	 "Q (Buck Shot)",	 		SPELL_CONE,	 			0,	 false,	 	false,	 				2,	 		250,	200,	false)
    Data:AddSkillData("Graves",		 	true,	 _W,	 700,	 "W (Smoke Screen)",	 	SPELL_CIRCLE,	 		0,	 false,	 	false,	 				1400,	 	300,	500,	false)
    Data:AddSkillData("Graves",		 	true,	 _E,	 580,	 "E (Quick Draw)",	 		SPELL_SELF_AT_MOUSE,	0,	 true,	 	true,	 				1450,	 	250,	200,	false)
    Data:AddSkillData("Caitlyn",		true,	 _Q,	 1300,	 "Q (Piltover Peacemaker)",	SPELL_LINEAR,			0,	 false,	 	false,	 				2.1,	 	625,	100,	true)
    Data:AddSkillData("Cassiopeia",     true,    _Q,     925,    "Q (Noxious Blast)",       SPELL_CIRCLE,           0,   false,     false,                  20000,      850,    125,     false)
    Data:AddSkillData("Cassiopeia",     true,    _W,     925,    "W (Miasma)",              SPELL_CIRCLE,           0,   false,     false,                  7250,       250,    250,    false)
    Data:AddSkillData("Cassiopeia",		true,	 _E,	 700,	 "E (Twin Fang)",	    	SPELL_TARGETED,	 		0,	 false,	 	false,	 				1.3,	 	250,	200,	false)
    Data:AddSkillData("Karthus",        true,    _Q,     925,    "Q (Lay Waste)",           SPELL_CIRCLE,           0,   false,     false,                  20000,      700,    140,     false)
    Data:AddSkillData("Karthus",        true,    _W,     1000,   "W (Wall of Pain)",        SPELL_CIRCLE,           0,   false,     false,                  20000,      250,    250,    false)
    Data:AddSkillData("Corki",		 	true,	 _Q,	 600,	 "Q (Phosphorus Bomb)",	 	SPELL_CIRCLE,			0,	 false,	 	false,	 				2,	 		200,	500,	false)
    Data:AddSkillData("Corki",		 	true,	 _R,	 1225,	 "R (Missile Barrage)",	 	SPELL_LINEAR_COL,		0,	 false,	 	false,	 				2,	 		200,	50,	 	true)
    Data:AddSkillData("Teemo",		 	true,	 _Q,	 580,	 "Q (Blinding Dart)",	 	SPELL_TARGETED,	 		0,	 false,	 	false,	 				2,	 		0,		200,	false)
    Data:AddSkillData("TwistedFate",	true,	 _Q,	 1200,	 "Q (Wild Cards)",	 		SPELL_LINEAR,	 		0,	 false,	 	false,	 				1.45,		250,	200,	false)
    Data:AddSkillData("Vayne",			true,	 _Q,	 750,	 "Q (Tumble)",	 			SPELL_SELF_AT_MOUSE,	0,	 true,	 	true,	 				1.45,		250,	200,	false)
    Data:AddSkillData("Vayne",			true,	 _R,	 580,	 "R (Final Hour)",	 		SPELL_SELF,	 			0,	 false,	 	true,	 				1.45,		250,	200,	false)
    Data:AddSkillData("MissFortune",	true,	 _Q,	 650,	 "Q (Double Up)",	 		SPELL_TARGETED,	 		0,	 true,	 	true,	 				1.45,		250,	200,	false)
    Data:AddSkillData("MissFortune",	true,	 _W,	 580,	 "W (Impure Shots)",	 	SPELL_SELF,	 			0,	 false,	 	true,	 				1.45,		250,	200,	false)
    Data:AddSkillData("MissFortune",	true,	 _E,	 800,	 "E (Make It Rain)",	 	SPELL_CIRCLE,	 		0,	 false,	 	false,	 				20000,	    500,	500,	false)
    Data:AddSkillData("Tristana",		true,	 _Q,	 580,	 "Q (Rapid Fire)",	 		SPELL_SELF,	 			0,	 false,	 	true,	 				1.45,	 	250,	200,	false)
    Data:AddSkillData("Tristana",		true,	 _E,	 550,	 "E (Explosive Shot)",		SPELL_TARGETED,			0,	 false,	 	false,	 				1.45,	 	250,	200,	false)
    Data:AddSkillData("Draven",			true,	 _E,	 1000,	 "E (Stand Aside)",	 		SPELL_LINEAR,			0,	 false,	 	false,	 				1.37,	 	300,	130,	false)   Data:AddSkillData("Draven",		true,	 _R,	 20000,	 "R (Whirling Death)",	 		SPELL_LINEAR,			0,	 false,	 	false,	 				2.00,	 	500,	160,	false)
    Data:AddSkillData("Kennen",			true,	 _Q,	 1050,	 "Q (Thundering Shuriken)",	SPELL_LINEAR_COL,		0,	 false,	 	false,	 				1.65,	 	180,	80,	 	true)
    Data:AddSkillData("Ashe",			true,	 _W,	 1200,	 "W (Volley)",	 			SPELL_LINEAR_COL,		0,	 false,	 	false,	 				2,	 		120,	85,	 	true)
    Data:AddSkillData("Syndra",			true,	 _Q,	 800,	 "Q (Dark Sphere)",	 		SPELL_CIRCLE,	 		0,	 false,	 	false,	 				20000,	    400,	100,	false)
    Data:AddSkillData("Jayce",			true,	 _Q,	 1600,	 "Q (Shock Blast)",	 		SPELL_LINEAR_COL,		0,	 false,	 	false,	 				2,	 		350,	90,	 	true)
    Data:AddSkillData("Thresh",			true,	 _Q,	 1100,	 "Q (Death Sentence)",	 	SPELL_LINEAR_COL,		0,	 false,	 	false,	 				1.9,	 	500,	65,	 	true)
    Data:AddSkillData("Nidalee",		true,	 _Q,	 1500,	 "Q (Javelin Toss)",	 	SPELL_LINEAR_COL,		0,	 false,	 	false,	 				1.3,		125,	60,	 	true)
    Data:AddSkillData("Varus",			true,	 _E,	 925,	 "E (Hail of Arrows)",	 	SPELL_CIRCLE,	 		0,	 false,	 	false,	 				1.75,		240,	235,	false)
    Data:AddSkillData("Varus",			true,	 _R,	 1075,	 "R (Ult)",	 				SPELL_LINEAR_COL,	 	0,	 false,	 	false,	 				1.95,		250,	80,	true)
	Data:AddSkillData("Quinn",			true,	 _Q,	 1050,	 "Q (Blinding Assault)",	SPELL_LINEAR_COL,		0,	 false,	 	false,	 				1.55,		220,	90,	 	true)
    Data:AddSkillData("LeeSin",			true,	 _Q,	 1100,	 "Q (Sonic Wave)",	 		SPELL_LINEAR_COL,		0,	 false,	 	false,	 				1.8,		250,	60,	 	true)
    Data:AddSkillData("Gangplank",		true,	 _Q,	 625,	 "Q (Parley)",	 			SPELL_TARGETED,	 		0,	 false,	 	false,	 				1.45,		250,	200,	false)
    Data:AddSkillData("Twitch",		 	true,	 _W,	 950,	 "W (Venom Cask)",	 		SPELL_CIRCLE,	 		0,	 false,	 	false,	 				1.4,		250,	275,	false)
    Data:AddSkillData("Darius",		 	true,	 _W,	 300,	 "W (Crippling Strike)",	SPELL_SELF,	 			0,	 true,	 	true,	 				2,	 		0,	 	200,	true)
    Data:AddSkillData("Hecarim",		true,	 _Q,	 300,	 "Q (Rampage)",	 			SPELL_SELF,	 			0,	 true,	 	true,	 				2,	 		0,	 	200,	true)
    Data:AddSkillData("Talon",		 	true,	 _Q,	 300,	 "Q (Noxian Diplomacy)",	SPELL_SELF,	 			0,	 true,	 	true,	 				2,	 		0,	 	200,	true)
	Data:AddSkillData("Talon",			true,	 _W,	 600,	 "W (Rake)",	 			SPELL_LINEAR,	 		0,	 false,	 	false,	 				2,	 		250,	220,	false)
	Data:AddSkillData("Talon",			true,	 _E,	 700,	 "E (Cutthroat)",	 		SPELL_TARGETED,	 	 	0,	 false,	 	false,	 				2,	 		0,	 	300,	false)
	Data:AddSkillData("Talon",			true,	 _R,	 300,	 "R (Shadow Assault)",	 	SPELL_SELF,	 			0,	 false,	 	true,	 				2,	 		0,	 	200,	false)
    
    Data:AddSkillData("Warwick",		true,	 _Q,	 300,	 "Q (Hungering Strike)",	SPELL_TARGETED,	 		0,	 true,	 	true,	 				2,	 		0,	 	200,	true)
    Data:AddSkillData("MonkeyKing",		true,	 _Q,	 300,	 "Q (Crushing Blow)",		SPELL_SELF,	 			0,	 true,	 	true,	 				2,	 		0,	 	200,	true)
    Data:AddSkillData("Poppy",		 	true,	 _Q,	 300,	 "Q (Devastating Blow)",	SPELL_SELF,	 			0,	 true,	 	true,	 				2,	 		0,	 	200,	true)
    Data:AddSkillData("Nautilus",		true,	 _W,	 300,	 "W (Titans Wrath)",	 	SPELL_SELF,	 			0,	 true,	 	true,	 				2,	 		0,	 	200,	true)
    Data:AddSkillData("Vi",		 		true,	 _E,	 300,	 "E (Excessive Force)",	 	SPELL_SELF,	 			0,	 true,	 	true,	 				2,	 		0,	 	200,	true)
    Data:AddSkillData("Rengar",		 	true,	 _Q,	 300,	 "Q (Savagery)",	 		SPELL_SELF,	 			0,	 true,	 	true,	 				2,	 		0,	 	200,	true)
    Data:AddSkillData("Trundle",		true,	 _Q,	 300,	 "Q (Chomp)",	 			SPELL_SELF,	 			0,	 true,	 	true,	 				2,	 		0,	 	200,	true)
    Data:AddSkillData("Leona",		 	true,	 _Q,	 300,	 "Q (Shield Of Daybreak)",	SPELL_SELF,	 			0,	 true,	 	false,	 				2,	 		0,	 	200,	true)
    Data:AddSkillData("Leona",		 	true,	 _W,	 300,	 "W (Eclipse)",				SPELL_SELF,	 			0,	 true,	 	true,	 				2,	 		0,	 	200,	true)
    Data:AddSkillData("Leona",		 	true,	 _E,	 700,	 "E (Zenith Blade)",		SPELL_LINEAR,	 		0,	 false,	 	false,	 				2.05,	 	250,	150,	false)
    Data:AddSkillData("Leona",		 	true,	 _R,	 1200,	 "R (Solar Flare)",			SPELL_CIRCLE,	 		0,	 false,	 	false,	 				20000,	 	625,	100,	false)
    Data:AddSkillData("Fiora",		 	true,	 _E,	 300,	 "E (Burst Of Speed)",	 	SPELL_SELF,	 			0,	 true,	 	true,	 				2,	 		0,	 	200,	true)
    Data:AddSkillData("Blitzcrank",		true,	 _Q,	 1050,	 "Q (Rocket Grab)",	 		SPELL_LINEAR_COL,       0,	 false,	 	false,	 				1.8,		250,	70, 	true)
    Data:AddSkillData("Blitzcrank",		true,	 _E,	 300,	 "E (Power Fist)",	 		SPELL_SELF,	 			0,	 true,	 	true,	 				2,	 		0,	 	200,	true)
    Data:AddSkillData("Shyvana",		true,	 _Q,	 300,	 "Q (Twin Blade)",	 		SPELL_SELF,	 			0,	 true,	 	true,	 				2,	 		0,	 	200,	true)
    Data:AddSkillData("Renekton",		true,	 _W,	 300,	 "W (Ruthless Predator)",	SPELL_SELF,	 			0,	 true,	 	true,	 				2,	 		0,	 	200,	true)
    Data:AddSkillData("Jax",		 	true,	 _W,	 300,	 "W (Empower)",	 			SPELL_SELF,	 			0,	 true,	 	true,	 				2,	 		0,	 	200,	true)
    Data:AddSkillData("XinZhao",		true,	 _Q,	 300,	 "Q (Three Talon Strike)",	SPELL_SELF,	 			0,	 true,	 	true,	 				2,	 		0,	 	200,	true)
    Data:AddSkillData("Nunu",			true,	 _E,	 300,	 "E (Snowball)",	 		SPELL_TARGETED,			0,	 false,	 	false,	 				1.45,		250,	200,	false)
    Data:AddSkillData("Khazix",			true,	 _Q,	 300,	 "Q (Taste Their Fear)",	SPELL_TARGETED,			0,	 true,	 	true,	 				1.45,		250,	200,	false)
    Data:AddSkillData("Nocturne",		true,	 _Q,	 1125,	 "Q (Duskbringer)", 	    SPELL_LINEAR,			0,	 false,	 	false,	 				1.40,		250,	60, 	false)
    Data:AddSkillData("Nocturne",		true,	 _E,	 425,	 "E (Unspeakable Horror)",  SPELL_TARGETED,			0,	 false,	 	false,	 				0,		    0,  	0,	    false)
    Data:AddSkillData("Diana",          true,    _Q,     1050,   "Q (Crescent Strike)",     SPELL_LINEAR,           0,   false,     false,                  1.6,        250,    195,    false)
    Data:AddSkillData("Diana",          true,    _W,     200,    "W (Pale Cascade)",        SPELL_SELF,             0,   false,     false,                  0,          0,      0,      false)
    Data:AddSkillData("Diana",          true,    _E,     250,    "E (Moonfall)",            SPELL_SELF,             0,   false,     false,                  0,          0,      0,      false)
	 Data:AddSkillData("Diana",          true,    _R,     825,    "R (Lunar Rush)",          SPELL_TARGETED,         0,   false,     false,                  0,          0,      0,      false) 
    Data:AddSkillData("Shen",			true,	 _Q,	 300,	 "Q (Vorpal Blade)",	 	SPELL_TARGETED,			0,	 false,	 	false,	 				1.45,		250,	200,	false)
    AutoCarry.Crosshair 	= _Crosshair(DAMAGE_PHYSICAL, MyHero.TrueRange, 0, false, false)
end